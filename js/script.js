$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.sf-menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')


// height

$(document).ready(function(){
  function boxPosition(){
    var windowHeight = $(window).height(),
        contentBox = $(".container_box"),
        headerH = $("header").height();
    if($(".container_box").length){
      $(contentBox).height(windowHeight-headerH);
    }
  }
  boxPosition();
  $(window).on("resize",boxPosition);
  
});

$(document).ready(function(){
      
      // language
      $(".language_btn").on('click',function(){
          $('.language_list').toggleClass('active');
          $('.language_btn').toggleClass('active');
      });

      $('.language_list_item').on('click',function(){
        var languageItem = $(this).attr('data-lan');
        $('.language_btn').text(languageItem);
      });

      $(document).click(function(event) {
        if ($(event.target).closest(".language_btn").length) return;
        $(".language_list").removeClass("active");
        $(".language_btn").removeClass("active");
        event.stopPropagation();
      });


      // menu
      $('.has_sub_menu').hover(
        function(){
        $('.bg_overlay').addClass('active');
        $('nav').addClass('active');
      },
      function(){
        $('.bg_overlay').removeClass('active');
        $('nav').removeClass('active');
      });

     // youtube
     $(".play_video").on('click',function(){
      var playerSrc = $(this).prev().attr('src');
      $(this).prev().attr('src', playerSrc + '&wmode=transparent&autoplay=1');
      $(this).addClass('active');
     });

     // swiperSlider
     if($('.swiper-container').length){
       var mySwiper = new Swiper('.swiper-container',{
          slidesPerView: 'auto',
          // loop:true,
          // mousewheelControl:true,
          // grabCursor: true,
          // paginationClickable: true,
          // scrollContainer: true,
          slidesPerView: 'auto',
          scrollbar: {
            dragSize: 35,
            snapOnRelease:true,
            container: '.swiper-scrollbar'
          },

        })
        $('.arrow-left').on('click', function(e){
          e.preventDefault()
          mySwiper.swipePrev()
        })
        $('.arrow-right').on('click', function(e){
          e.preventDefault()
          mySwiper.swipeNext()
        })
      }

      //main slider 
      if($('#main_slider').length){
        $('#main_slider').royalSlider({
          arrowsNav: true,
          loop: false,
          keyboardNavEnabled: true,
          controlsInside: false,
          imageScaleMode: 'fill',
          arrowsNavAutoHide: true,
          autoScaleSlider: true, 
          // autoScaleSliderWidth: 960,     
          // autoScaleSliderHeight: 350,
          controlNavigation: 'bullets',
          thumbsFitInViewport: false,
          navigateByClick: true,
          startSlideId: 0,
          autoPlay: {
            enabled: true,
            delay:10000
          },
          transitionType:'fade',
          globalCaption: false,
          deeplinking: {
            enabled: true,
            pauseOnHover:false,
            change: false
          },
          /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
          // imgWidth: "100%",
          // imgHeight: "100%"
        });
      }
      

      // width markets_tips_text
      $('.markets_tips_text').each(function(){
        var _this = $(this),
            widthMarketsItem = _this.parents('.markets_item').width();
        _this.width(widthMarketsItem-44);
      });

      function info1Height(){
        $('.info1_text_box').each(function(){
          var info1Height = $(this).parents('.info1_box').height();
          $(this).height(info1Height);
        });
      } 
      $(window).load(function(){
        info1Height();
      });

      // tabs
      $('.tabs_link').on('click',function(){
        var $this = $(this);
            tabsContentId = $this.attr('data-tabs');
            $thisIndex = $(tabsContentId).index();
        $(tabsContentId).addClass('active').siblings().removeClass('active');
        $(".tabs_list")
        .find('.tabs_link')
        .eq($thisIndex)
        .addClass('current')
        .siblings()
        .removeClass('current');
        info1Height();
      });

    });
